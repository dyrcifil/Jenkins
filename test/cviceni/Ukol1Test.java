package cviceni;

import org.testng.Assert;
import org.testng.annotations.Test;

public class Ukol1Test {

	@Test (expectedExceptions = IllegalArgumentException.class)
	public void testFaktorial1() {
		Assert.assertEquals(Ukol1.faktorial(0), 1);
		Assert.assertEquals(Ukol1.faktorial(1), 1);
		Assert.assertEquals(Ukol1.faktorial(2), 2);
		Assert.assertEquals(Ukol1.faktorial(3), 6);
		Assert.assertEquals(Ukol1.faktorial(5), 120);
		
		Assert.assertTrue(Ukol1.faktorial(20)<0);
		
		Ukol1.faktorial(-1);
	}

	@Test (groups = "todo")
	public void testFaktorial2() {
		throw new UnsupportedOperationException("Zatim neimplementovano");
	}

	@Test(groups = "todo")
	public void testFaktorial3() {
		throw new UnsupportedOperationException("Zatim neimplementovano");
	}
}
