package cviceni;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class StringBuildersTest {

	StringBuilder sbuilder;
	StringBuffer sbuffer;
	
	@BeforeClass
	public void init(){
		sbuilder = new StringBuilder();
		sbuffer = new StringBuffer();
	}
	
	@Test(dataProvider = "letters", threadPoolSize = 5, invocationCount=1000)
	public void StringBuilderTest(String letter){
		sbuilder.append(letter);
	}
	
	@Test(dataProvider = "letters", threadPoolSize = 5, invocationCount=1000)
	public void StringBufferTest(String letter){
		sbuffer.append(letter);
	}
	
	@Test
	public void result(){
		
		Assert.assertEquals(sbuilder.length(), 10000);
		Assert.assertEquals(sbuffer.length(), 10000);
		
	}
	
	@DataProvider(name="letters")
	public Object[][] provideObjects(){
		return new Object[][] {{ "a"}, {"b"}, {"c"}, {"d"}, {"e"} , {"f"}, {"g"}, {"h"}, {"i"}, {"j"}};
	}
}
